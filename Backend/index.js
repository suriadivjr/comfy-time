'use strict';
const express = require('express')
const bodyParser = require('body-parser')
const sqlite3 = require('sqlite3').verbose()
const database = new sqlite3.Database('./data.db')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const app = express()
const router = express.Router()
const SECRET_KEY = 'secretkey23456'

router.use(bodyParser.urlencoded({ extended: false }))
router.use(bodyParser.json())

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

router.post('/register', (req, res) => {
  const name = req.body.name
  const email = req.body.email
  const password = bcrypt.hashSync(req.body.password)

  createUser([name, email, password], (err) => {
    if (err) return res.status(500).send('Server Error!')
    findUserByEmail(email, (err, user) => {
      if (err) return res.status(500).send('Server Error!')
      const expiresIn = 24 * 60 * 60
      const accessToken = jwt.sign({ id: user.id }, SECRET_KEY, {
        expiresIn: expiresIn
      })
      res.status(200).send({
        'user': user, 'access_token': accessToken, 'expires_in': expiresIn
      })
    })  
  })
})

router.post('/login', (req, res) => {
  const email = req.body.email
  const password = req.body.password

  findUserByEmail(email, (err, user) => {
    if (err) return res.status(500).send('Server error!')
    if (!user) return res.status(404).send('User not found!')
    const result = bcrypt.compareSync(password, user.password)
    if (!result) return res.status(401).send('Password is not valid')

    const expiresIn = 24 * 60 * 60
    const accessToken = jwt.sign({ id: user.id }, SECRET_KEY, {
      expiresIn: expiresIn
    })
    res.status(200).send({
      'user': user, 'access_token': accessToken, 'expires_in': expiresIn
    })
  })
})

router.get('/', (req, res) => {
  res.status(200).send('This is an authentication server')
})

const createUserTable = () => {
  const sqlQuery = `
    CREATE TABLE IF NOT EXISTS users (
      id integer PRIMARY KEY,
      name text,
      email text UNIQUE,
      password text
    )`

  return database.run(sqlQuery)
}

const findUserByEmail = (email, cb) => {
  return database.get(
    `SELECT * FROM users WHERE email = ?`, [email], (err, row) => {
      cb(err, row)
    }
  )
}

const createUser = (user, cb) => {
  return database.run(
    `INSERT INTO users (name, email, password) VALUES (?,?,?)`,
    user, (err) => {
      cb(err)
    }
  )
}

createUserTable()

app.use(router)
const port = process.env.PORT || 3000
const server = app.listen(port, () => {
  console.log('Server listening at http://localhost:' + port)
})
