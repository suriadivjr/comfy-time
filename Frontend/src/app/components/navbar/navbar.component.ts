import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  isLoggedIn: boolean;
  username: string;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.authService.isAuthenticated().subscribe((status) => {
      this.isLoggedIn = status;
    });
  }


  toggleLogIn() {
    this.authService.logout();
  }

}
