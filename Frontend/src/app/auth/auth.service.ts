import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { tap } from 'rxjs/operators';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

import { User } from '../models/user';
import { JwtResponse } from '../models/jwt-response';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private httpClient: HttpClient) { }

  AUTH_SERVER = 'http://localhost:3000';
  authSubject = new BehaviorSubject(false);
  private userSubject = new Subject<string>();


  register(user: User): Observable<JwtResponse> {
    return this.httpClient.post<JwtResponse>(
      `${this.AUTH_SERVER}/register`, user
      )
      .pipe(tap((res: JwtResponse) => {
        if (res.user) {
          localStorage.setItem('ACCESS_TOKEN', res.user.access_token);
          localStorage.setItem('EXPIRES_IN', res.user.expires_in);
          this.authSubject.next(true);
          this.userSubject.next(res.user.name);
        }
      })
    );
  }

  login(user: User): Observable<JwtResponse> {
    return this.httpClient.post<JwtResponse>(
      `${this.AUTH_SERVER}/login`, user
      )
      .pipe(tap(async (res: JwtResponse) => {
        if (res.user) {
          localStorage.setItem('ACCESS_TOKEN', res.user.access_token);
          localStorage.setItem('EXPIRES_IN', res.user.expires_in);
          this.authSubject.next(true);
        }
      })
    );
  }

  logout() {
    localStorage.removeItem('ACCESS_TOKEN');
    localStorage.removeItem('EXPIRES_IN');
    this.authSubject.next(false);
  }

  isAuthenticated(): Observable<boolean> {
    return this.authSubject.asObservable();
  }

  getUserInfo(): Observable<string> {
    return this.userSubject.asObservable();
  }

}
